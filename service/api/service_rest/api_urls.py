from django.urls import path
from .views import api_list_appointments, api_show_appointment, api_status, api_show_status

urlpatterns = [
    path('appointments/', api_list_appointments, name='list_appointments'),
    path('appointments/<int:pk>', api_show_appointment, name='show_appointment'),
    path('appointments/status', api_status, name='list_status'),
    path('appointments/status/<int:pk>', api_show_status, name='show_status')
]
