from django.db import models
from django.urls import reverse

class TechnicianVO(models.Model):
    import_href = models.CharField(max_length=100, unique=True)
    employee_name = models.CharField(max_length=50)

    def __str__(self):
        return self.employee_name

class Status(models.Model):
    state = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.state

    def get_api_url(self):
        return reverse("show_status", kwargs={"pk": self.id})


class Appointment(models.Model):
    owner = models.CharField(max_length=50)
    date = models.DateTimeField()
    reason = models.TextField(null=True)
    vin = models.CharField(max_length=17)
    technician = models.ForeignKey(
        TechnicianVO,
        related_name="technician",
        on_delete=models.CASCADE
    )
    status = models.ForeignKey(
        Status,
        related_name="status",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.vin
