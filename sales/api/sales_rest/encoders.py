from .models import (
    SalesPerson,
    AutomobileVO, Sales,
    PotentialCustomer)
from common.json import ModelEncoder

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "import_href","id"]


class PotentialCustomerEncoder(ModelEncoder):
    model = PotentialCustomer
    properties = ["name", "address", "phone", "id"]


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["employee_name", "employee_number", "id"]

class SalesEncoder(ModelEncoder):
    model = Sales
    properties = ["price", "automobile", "customer","sales_person", "id"]

    encoders = {
        "automobile": AutomobileVOEncoder(),
        "customer": PotentialCustomerEncoder(),
        "sales_person": SalesPersonEncoder()
    }
