from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique = True)
    import_href = models.CharField(max_length=250, unique=True)

    def __str__(self):
        return self.vin


class SalesPerson(models.Model):
    employee_name = models.CharField(max_length=100)
    employee_number = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return self.employee_name

    def get_api_url(self):
        return reverse("show_sale_person", kwargs={"pk": self.id})



class PotentialCustomer(models.Model):
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=200)
    phone = models.CharField(max_length=200)

    def __str__(self):
        return self.name



class Sales(models.Model):

    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="sales_person",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        PotentialCustomer,
        related_name='customer',
        on_delete=models.CASCADE
    )
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobile",
        on_delete=models.CASCADE,
    )
    price = models.CharField(max_length=50)

    def __str__(self):
        return self.customer
