# CarCar

Team:

* Malcolm Pei - Appointment Microservice
* Yannis Pashalidis - Sales Microservice

## Design

How to use:
- To get started, create an account to login into the system. The username needs to be unique.
- You will be met with the main page once logged in.
- You can use the [Get Started] button to quickly start adding inventory data to use the rest of the application.
- Once you have inventory, you now need to add employees for your services.
- Now you can start using the application without worries of any missing foreign keys as input
    - Note: The application automatically adds "Submitted" into the appointment/status microservice

## Service microservice

After deciding how these 3 microservices would interact, I created 3 models:
- Status
    - GET/POST: http://localhost:8080/api/appointments/status
    - GET/DELETE/PUT: http://localhost:8080/api/appointments/status/<int:pk>
    - PUT: {"state": "Submitted"}
- TechnicianVO
    - Polls from the sales microservice at: http://sales-api:8000/api/salesperson/
- Appointment
    - GET/POST: http://localhost:8080/api/appointments
    - GET/DELETE/PUT: http://localhost:8080/api/appointments/<int:pk>
    - PUT: {
	"owner": "Joe Smith",
	"date": "2053-01-10T11:10:00+00:00",
	"reason": "Why your automobile needs fixing",
	"vin": "HASDJ1230212SASD",
	"technician": "/api/salesperson/1",
	"status": "Submitted"
    }
    - DELETE: http://localhost:8080/api/appointments/<int:pk>

For the front-end appointment application to work as intended, it is important to first create Status' as it is a foreign key to the appointment model. If you go look at ghi/src/components/AppointmentForm.js on line 7: You will notice the form data will add a status when creating an appointment. The current default is "Submitted".
I've also implemented a function that automatically posts the status:Submitted if there are no status in the database.


## Sales microservice

For my models I created:

SalesPerson:

which consists of the sales persons name and employee number. A couple view functions were made for SalesPerson. One of them was created with methods to create a salesperson, edit a salesperson and delete a salesperson.  The other view was created to show the list of them and it pulls data from the created salesperson to add them to the list accordingly.

PotentialCustomer:

which consists of the customers name, address, and phone number. A couple view functions were made for PotentialCustomer. One of them was created with methods to create a customer, edit a customer and delete a customer.  The other view was created to show the list of them and it pulls data from the created customer to add them to the list accordingly.

Sales:

which consists of the sales person name, customer, automobile and price. A couple view functions were made for Sales. One of them was created with methods to create a sale, edit a sale and delete a sale.  The other view was created to show the list of them and it pulls data from the created sale to add them to the list accordingly.

AutomobileVO:

this is a representation of a vehicle object in the database.

The rest is on the front end for best implementation for the user, the backend made it possible to pull any data with ease.

All of these models, views, poller and encoders for each, come together in the front end and makes it so the user can edit, add, delete data for each component and mixes all of the other microservices together and they pull from each other to gather all data and puts them together.
Explain your models and integration with the inventory
microservice, here.

## Accounts microservice

Added an extra microservice to deal with user login information in order to create the experience of authentication
- GET/POST: http://localhost:8200/api/accounts
- GET/DELETE: http://localhost:8200/api/accounts/<int:pk>
This is a minimal extension to the application without any real authentication yet
