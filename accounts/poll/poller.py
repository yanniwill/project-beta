import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "accounts_project.settings")
django.setup()

from accounts_api.models import TechnicianVO, AutoVO

def get_salesPerson():
    response = requests.get('http://sales-api:8000/api/salesperson/')
    content = json.loads(response.content)
    for person in content['employees']:
        TechnicianVO.objects.update_or_create(
            import_href=person['href'],
            defaults={"employee_name":person['employee_name']}
        )

def get_autos():
    response = requests.get('http://inventory-api:8000/api/automobiles/')
    content = json.loads(response.content)
    for auto in content["autos"]:
        AutoVO.objects.update_or_create(
            import_href = auto['href'],
            defaults={"vin":auto['vin']}
        )

def poll():
    while True:
        print('Service poller polling for data')
        try:
            get_salesPerson()
            get_autos()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
