from django.urls import path
from .views import api_account, api_show_account

urlpatterns = [
    path('accounts/', api_account, name="account_list"),
    path('account/<int:pk>', api_show_account, name="account_detail")
]
