from django.contrib import admin
from .models import Account, Appointment, TechnicianVO, AutoVO


admin.site.register(Account)
admin.site.register(Appointment)
admin.site.register(TechnicianVO)
admin.site.register(AutoVO)
