from django.db import models


class Account(models.Model):
    username = models.CharField(max_length=50, unique=True)
    password = models.CharField(max_length=50)
    email = models.EmailField(null=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

class TechnicianVO(models.Model):
    import_href = models.CharField(max_length=50)
    employee_name = models.CharField(max_length=50)

class AutoVO(models.Model):
    import_href = models.CharField(max_length=50)
    vin = models.CharField(max_length=17)

class Appointment(models.Model):
    owner = models.ForeignKey(
        Account,
        related_name="account",
        on_delete=models.CASCADE
    )
    date = models.DateTimeField()
    reason = models.TextField(null=True)
    vin = models.CharField(max_length=17)
    technician = models.ForeignKey(
        TechnicianVO,
        related_name="technician",
        on_delete=models.CASCADE
    )
