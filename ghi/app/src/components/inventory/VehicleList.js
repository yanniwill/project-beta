import { useEffect, useState } from "react"
import { BsFillHeartFill, BsHeart } from "react-icons/bs"

function VehicleList() {
    const [vehicles, setVehicles] = useState([])
    const [filtered, setQuery] = useState()
    const getData = async () => {
        const url = 'http://localhost:8100/api/models'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setVehicles(data.models)
        }
    }
    useEffect(() => {
        getData()
    }, [])
    // Handle search query
    const handleSearch = event => {
        SearchByName(event.target.value, vehicles)
    }
    // Handle search filter
    function SearchByName(q, list) {
        if (!q) {
            setQuery(list)
        }
        else {
            let result = []
            vehicles.map(v => {
                if (v.name.toUpperCase().includes(q.toUpperCase())) {
                    result.push(v)
                }
                if (v.manufacturer.name.toUpperCase().includes(q.toUpperCase())) {
                    result.push(v)
                }
            })
            let unique = [...new Set(result)]
            setQuery(unique)
        }
    }
    // If you use filtered list instead
    function FilteredList() {
        if (filtered) {
            return filtered
        }
        return vehicles
    }
    const [like, setLike] = useState({undefined:false})
    const handleLike = event => {
        if (like[event.currentTarget.value] === true) {
            setLike({
                ...like,
                [event.currentTarget.value]:false
            })
        }
        else {
            setLike({
                ...like,
                [event.currentTarget.value]:true
            })
        }
    }
    const handleLikeIcon = (id) => {
        if (like[id] === true) {
            return (
                <BsFillHeartFill/>
            )
        }
        else {
            return (
                <BsHeart/>
            )
        }
    }
    return (
        <div className="container">
            <div className="row">
                <h1 className="col"><strong>VEHICLE MODELS</strong></h1>
                <div className="col form-floating text-end p-2">
                    <input onChange={handleSearch} type="search" className="form-control"/>
                    <label>Search...</label>
                </div>
            </div>
            <div className="row p-2 justify-content-center">
                {FilteredList().map(v => { // v.picture_url v.name v.manufacturer.name
                    return (
                        <div key={v.id} className="card col-4 rounded m-2" style={{height:"350px", width:"400px"}}>
                            <div className="row">
                                <div className="col">
                                    <h3><strong>{(v.manufacturer.name).toUpperCase()}</strong></h3>
                                    <h4>{v.name}</h4>
                                </div>
                                <div className="col text-end">
                                    <button onClick={handleLike} className="btn" value={v.id}>
                                        {handleLikeIcon(v.id)}
                                    </button>
                                </div>
                            </div>
                            <div style={{height:"200px", overflow:"hidden"}}>
                                <img src={v.picture_url} className="card-img"/>
                            </div>
                            <div className="text-muted">Fully driveable no steering wheel experience</div>
                            <div className="text-muted">Hands-free and AI-free</div>
                        </div>
                    )
                })}
            </div>
        </div>
    )
}
export default VehicleList
