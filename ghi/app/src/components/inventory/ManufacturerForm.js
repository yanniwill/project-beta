import { useState } from "react"
import { useNavigate } from "react-router-dom"
import { NavLink } from "react-router-dom"

function ManufacturerForm() {
    const [formData, setFormData] = useState({})
    const navigate = useNavigate()
    const defaultPhoto = "https://content.tgstatic.co.nz/webassets/assets/images/default-car.png"
    const defaultText = "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
    const backgroundPhoto = "https://cdn.motor1.com/images/mgl/pb01ZJ/s3/best-car-paint-colors-2022.jpg"
    const handleFormData = event => {
        setFormData({
            ...formData,
            [event.target.name]:event.target.value
        })
    }
    const handleSubmit = event => {
        event.preventDefault()
        postData()
    }
    const postData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/'
        const fetchData = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {"Content-Type":"application/json"}
        }
        const response = await fetch(url, fetchData)
        if (response.ok) {
            const newResponse = await response.json()
            const manufacturerSuccess = document.querySelector('#manufacturer-success')
            const manufacturerForm = document.querySelector('#manufacturer-form')
            manufacturerSuccess.classList.remove('d-none')
            manufacturerForm.classList.add('d-none')
        }
    }
    return (
        <div>
            <div className="container">
                <div>
                    <h1 className="position-absolute start-0 p-5 m-5"><strong>
                        <div>ADD A </div>
                        <div>MANUFACTURER</div>
                        <div>TO THE</div>
                        <div>DATABASE</div>
                    </strong></h1>
                </div>
            </div>
            <div className="container position-fixed w-50 start-50 p-5 m-5">
                <div className="d-none" id="manufacturer-success">
                    <div className="alert alert-success">Successfully added manufacturer</div>
                    <div className="text-center">
                        <NavLink to="/inventory/models/new" className="btn btn-primary">Continue</NavLink>
                    </div>
                </div>
                <div id="manufacturer-form">
                    <div className="p-2">
                        <form onSubmit={handleSubmit}>
                            <div className="p-2">
                                <div className="form-floating shadow">
                                    <input onChange={handleFormData} className="form-control" required name="name"></input>
                                    <label>Brand name</label>
                                </div>
                                <div className="text-center p-2">
                                    <button className="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <img src={backgroundPhoto} className="vh-100"/>
        </div>
    )
}

export default ManufacturerForm
