import { useState, useEffect } from "react"
import AccountForm from "./AccountForm"

function Login({setToken}) {
    const [formData, setFormData] = useState()
    const [users, setUsers] = useState()
    const [verified, setVerified] = useState(false)
    const getdata = async () => {
        const response = await fetch('http://localhost:8200/api/accounts')
        if (response.ok) {
            const data = await response.json()
            setUsers(data.accounts)
        }
    }
    useEffect(() => {
        getdata()
    },[])
    const handleForm = event => {
        setFormData({
            ...formData,
            [event.target.name]:event.target.value
        })
    }
    const checkAccount = () => {
        for (let user of users) {
            if (user.username === formData.username && user.password === formData.password) {
                setToken({
                    "first_name":user.first_name,
                    "last_name":user.last_name
                })
                setVerified(true)
            }
        }
    }
    const usernameTag = document.querySelector('#username')
    const passwordTag = document.querySelector('#password')
    const newAccount = document.querySelector('#new-account-view')
    const loginView = document.querySelector('#login-view')

    const handleSubmit = event => {
        event.preventDefault()
        checkAccount()
        if (verified === false) {
            usernameTag.classList.add('btn-outline-danger')
            passwordTag.classList.add('btn-outline-danger')
        }
    }
    const handleCreateAccount = () => {
        newAccount.classList.remove('d-none')
        loginView.classList.add('d-none')
    }
    return (
        <div>
            <div className="container p-2 w-50" id="login-view">
                <form onSubmit={handleSubmit} className="container">
                    <h1 className="text-center">Login</h1>
                    <div className="form-floating">
                        <input onChange={handleForm} className="form-control" name="username" id="username" required></input>
                        <label>username</label>
                    </div>
                    <br></br>
                    <div className="form-floating">
                        <input onChange={handleForm} type="password" className="form-control" name="password" id="password" required></input>
                        <label>password</label>
                    </div>
                    <div className="text-center p-2">
                        <button className="btn btn-primary m-2">Login</button>
                    </div>
                </form>
                <div className="text-center">
                    <button className="btn btn-warning m-2">Forgot password</button>
                    <button onClick={handleCreateAccount} className="btn btn-success m-2">Create account</button>
                </div>
            </div>
            <AccountForm/>
        </div>
    )
}

export default Login
