import { useState } from "react"

function AccountForm() {
    const [formData, setFormData] = useState({})
    const accountSuccess = document.querySelector('#account-success')
    const accountForm = document.querySelector('#account-form')
    const loginView = document.querySelector('#login-view')
    const newAccount = document.querySelector('#new-account-view')
    const handleForm = event => {
        setFormData({
            ...formData,
            [event.target.name]:event.target.value
        })
    }
    const handleSubmit = event => {
        event.preventDefault()
        postData()
    }
    const postData = async () => {
        const url = 'http://localhost:8200/api/accounts/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {"Content-Type":"application/json"}
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const newResponse = await response.json()
            accountForm.classList.add('d-none')
            accountSuccess.classList.remove('d-none')
        }
    }
    const handleBackButton = () => {
        accountSuccess.classList.add('d-none')
        loginView.classList.remove('d-none')
    }
    const handleReturnButton = () => {
        loginView.classList.remove('d-none')
        newAccount.classList.add('d-none')
    }
    return (
        <div>
            <div className="container w-50 d-none" id="new-account-view">
                <div className="text-center d-none" id="account-success">
                    <div className="alert alert-success">Successfully created a new account</div>
                    <button onClick={handleBackButton} className="btn btn-success">Back</button>
                </div>
                <div className="container" id="account-form">
                    <form onSubmit={handleSubmit}>
                        <h1 className="text-center">New Account</h1>
                        <div className="row">
                            <div className="form-floating col p-2">
                                <input onChange={handleForm} className="form-control" name="username" required></input>
                                <label>username</label>
                            </div>
                            <div className="form-floating col p-2">
                                <input onChange={handleForm} type="password" className="form-control" name="password" required></input>
                                <label>password</label>
                            </div>
                        </div>
                        <div className="row">
                            <div className="form-floating col p-2">
                                <input onChange={handleForm} className="form-control" name="first_name" required></input>
                                <label>First name</label>
                            </div>
                            <div className="form-floating col p-2">
                                <input onChange={handleForm} className="form-control" name="last_name" required></input>
                                <label>Last name</label>
                            </div>
                        </div>
                        <div className="row">
                            <div className="form-floating col p-2">
                                <input onChange={handleForm} type="email" className="form-control" name="email" required></input>
                                <label>Email</label>
                            </div>
                        </div>
                        <div className="text-center">
                            <button className="btn btn-success">Create</button>
                        </div>
                    </form>
                    <div className="text-center p-2">
                        <button onClick={handleReturnButton} className="btn btn-warning">Return</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AccountForm
