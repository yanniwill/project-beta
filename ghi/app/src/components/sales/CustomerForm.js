import React, { useState } from 'react';
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';
import { NavLink } from 'react-router-dom';


function CustomerForm() {
    // create a state variable to hold the form data
    const [formData, setFormData] = useState({
        name: "",
        address: "",
        phone: ""
    });

    // handle changes in the form inputs
    const handleChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        });
    }

    // handle form submit
    const handleSubmit = async (event) => {
        event.preventDefault();
        try {
            // specify the url to create a new customer
            const customerUrl = 'http://localhost:8090/api/customers/';
            // make a POST request to the server with the form data
            const fetchConfig = {
                method: "POST",
                body: JSON.stringify(formData),
                headers: {'Content-Type':'application/json'}
            }
            const response = await fetch(customerUrl, fetchConfig);
            if (response.ok) {
                alert('customer created successfully');
                // reset the form data
                setFormData({
                    name: "",
                    address: "",
                    phone: ""
                });
            } else {
                throw new Error('Something went wrong');
            }
        } catch (error) {
            console.error(error);
        }
    }

    // render the component
    return (
        <div className='container shadow p-2'>
            <Form onSubmit={handleSubmit}>
                <FormGroup>
                    <Label for="name">Name</Label>
                    <Input type="text" name="name" id="name" value={formData.name} onChange={handleChange} />
                </FormGroup>
                <FormGroup>
                    <Label for="address">Address</Label>
                    <Input type="text" name="address" id="address" value={formData.address} onChange={handleChange} />
                </FormGroup>
                <FormGroup>
                    <Label for="phone">Phone</Label>
                    <Input type="text" name="phone" id="phone" value={formData.phone} onChange={handleChange} />
                </FormGroup>
                <Button color="success" type="submit" className='mx-2'>Submit</Button>
                <NavLink type="button" className='btn btn-warning' to="/sales">Back</NavLink>
            </Form>
        </div>
    )
};

export default CustomerForm;
