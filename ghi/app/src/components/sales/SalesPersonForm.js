import React, { useState } from 'react';
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';
import { NavLink } from 'react-router-dom';


function SalesPersonForm() {
    // create a state variable to hold the form data
    const [formData, setFormData] = useState({
        employee_name: "",
        employee_number: "",
    });

    // handle changes in the form inputs
    const handleChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        });
    }

    // handle form submit
    const handleSubmit = async (event) => {
        event.preventDefault();
        try {
            // specify the url to create a new salesperson
            const salespersonUrl = 'http://localhost:8090/api/salesperson/';
            // make a POST request to the server with the form data
            const fetchConfig = {
                method: "POST",
                body: JSON.stringify(formData),
                headers: {'Content-Type':'application/json'}
            }
            const response = await fetch(salespersonUrl, fetchConfig);
            if (response.ok) {
                alert('Salesperson created successfully');
                // reset the form data
                setFormData({
                    employee_name: "",
                    employee_number: "",
                });
            } else {
                throw new Error('Something went wrong');
            }
        } catch (error) {
            console.error(error);
        }
    }

    // render the component
    return (
        <div className='container shadow p-2'>
            <Form onSubmit={handleSubmit}>
                <FormGroup>
                    <Label for="employee_name">Name</Label>
                    <Input type="text" name="employee_name" id="employee_name" value={formData.employee_name} onChange={handleChange} />
                </FormGroup>
                <FormGroup>
                    <Label for="employee_number">Employee Number</Label>
                    <Input type="text" name="employee_number" id="employee_number" value={formData.employee_number} onChange={handleChange} />
                </FormGroup>
                <Button color="success" type="submit" className='mx-2'>Submit</Button>
                <NavLink type="button" className='btn btn-warning' to="/sales">Back</NavLink>
            </Form>
        </div>
    )
};

export default SalesPersonForm;
