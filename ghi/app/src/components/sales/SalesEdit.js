import { useState, useEffect } from "react"
import { NavLink, useParams } from "react-router-dom"



function SalesEdit() {
    const { id } = useParams();
    const [employees, setEmployees] = useState([])
    const [customers, setCustomers] = useState([])
    const [automobiles, setAutos] = useState([])
    const [formData, setFormData] = useState({})

    const saleForm = document.querySelector('#sale-form')
    const saleSuccess = document.querySelector('#sale-success')
    const getData = async () => {
        const employeeUrl = 'http://localhost:8090/api/salesperson'
        const customerUrl = 'http://localhost:8090/api/customers'
        const automobileUrl = 'http://localhost:8100/api/automobiles'
        const employeeResponse = await fetch(employeeUrl)
        const customerResponse = await fetch(customerUrl)
        const automobileResponse = await fetch(automobileUrl)
        if (employeeResponse.ok) {
            const data = await employeeResponse.json()
            setEmployees(data.employees)
        }
        if (customerResponse.ok) {
            const data = await customerResponse.json()
            setCustomers(data.customers)
        }
        if (automobileResponse.ok) {
            const data = await automobileResponse.json()
            setAutos(data.autos)
        }
    }
    useEffect(() => {
        getData()
    },[])
    const putData = async () => {
        const url = `http://localhost:8090/api/sales/${id}`
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify(formData),
            headers: {"Content-Type":"application/json"}
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const newResponse = await response.json()
            saleForm.classList.add('d-none')
            saleSuccess.classList.remove('d-none.3')
        }
    }
    const handleFormData = event => {
        setFormData({
            ...formData,
            [event.target.name]:event.target.value
        })

    }
    const handleSubmit = event => {
        event.preventDefault()
        putData()

    }
    return (
        <div>
            <div className="container">
                <div className="text-center d-none" id="sale-success">
                    <div className="alert alert-success">Successfully Changed</div>
                </div>
                <div className="" id="sale-form">
                    <h1>Edit Sale</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="row p-2">
                            <div className="form-floating col">
                                <select onChange={handleFormData} name="customer" className="form-select">
                                    <option value=""></option>
                                    {customers.map(customer => {
                                        return (
                                            <option key={customer.id} value={customer.id}>{customer.name}</option>
                                        )
                                    })}
                                </select>
                                <label>Customer</label>
                            </div>
                            <div className="form-floating col">
                                <select onChange={handleFormData} name="sales_person" className="form-select">
                                    <option value=""></option>
                                    {employees.map(employee => {
                                        return (
                                            <option key={employee.href} value={employee.employee_name}>{employee.employee_name}</option>
                                        )
                                    })}
                                </select>
                                <label>Employee</label>
                            </div>
                        </div>
                        <div className="row p-2">
                            <div className="form-floating col">
                                <select onChange={handleFormData} name="automobile" className="form-select">
                                    <option value=""></option>
                                    {automobiles.map(auto => {
                                        return (
                                            <option key={auto.href} value={auto.href}>{auto.vin}</option>
                                        )
                                    })}
                                </select>
                                <label>Automobile</label>
                            </div>
                            <div className="col">
                                <div className="form-floating">
                                    <input onChange={handleFormData} name="price" type="number" step="0.0.1" className="form-control" required/>
                                    <label>Price</label>
                                </div>
                            </div>
                        </div>
                        <div className="form-group text-center">
                            <NavLink to="/sales" className="btn btn-danger mr-2">Cancel</NavLink>
                            <button type="submit" className="btn btn-success">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
                                }
export default SalesEdit
