import React, { useState, useEffect } from 'react';
import { Table, Button } from 'reactstrap';
import { Link, NavLink } from 'react-router-dom';

function CustomerList() {
    const [customers, setCustomers] = useState([]);

  useEffect(() => {
    // Use a function to fetch the data from the server
    fetchCustomers();
  }, []);

  const fetchCustomers = async () => {
    // Use fetch API to get the data from the server
    const response = await fetch('http://localhost:8090/api/customers/');
    const data = await response.json();
    setCustomers(data.customers);
  }
  const handleDelete = async (id) => {
    try {
        const response = await fetch(`http://localhost:8090/api/customers/${id}`, {
            method: 'DELETE'
        });
        if(response.ok) {
            // Remove the deleted customer from the state
            setCustomers(customers.filter(customer => customer.id !== id));
        }
    } catch(error) {
        console.error(error);
    }
}


return (
    <div className='container p-2'>
      <h1>Customers</h1>
      <Table>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Address</th>
            <th>Phone</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {customers.map((customer, index) => (
            <tr key={customer.id}>
              <th scope="row">{index + 1}</th>
              <td>
                  {customer.name}
              </td>
              <td>
                {customer.address}
              </td>
              <td>
                  {customer.phone}
              </td>
              <td>
                <Link to={`/sales/customers/edit/${customer.id}`}>
                  <Button color="primary">Edit</Button>
                </Link>
                <Button color="danger" onClick={() => handleDelete(customer.id)}>Delete</Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
      <NavLink type="button" className='btn btn-warning' to="/sales">Back</NavLink>
    </div>
  );

}

export default CustomerList;
