import React, { useState, useEffect } from 'react';
import { Table, Button } from 'reactstrap';
import { Link, NavLink } from 'react-router-dom';

function SalesPersonList() {
  const [salespersons, setSalespersons] = useState([]);

  useEffect(() => {
    // Use a function to fetch the data from the server
    fetchSalespersons();
  }, []);

  const fetchSalespersons = async () => {
    // Use fetch API to get the data from the server
    const response = await fetch('http://localhost:8090/api/salesperson/');
    const data = await response.json();
    setSalespersons(data.employees);
  }
  const handleDelete = async (id) => {
    try {
        const response = await fetch(`http://localhost:8090/api/salesperson/${id}`, {
            method: 'DELETE'
        });
        if(response.ok) {
            // Remove the deleted salesperson from the state
            setSalespersons(salespersons.filter(salesperson => salesperson.id !== id));
        }
    } catch(error) {
        console.error(error);
    }
}


return (
    <div className='container p-2'>
        <h1>Employees</h1>
      <Table>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Employee Number</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {salespersons.map((salesperson, index) => (
            <tr key={salesperson.id}>
              <th scope="row">{index + 1}</th>
              <td>
                  {salesperson.employee_name}
              </td>
              <td>{salesperson.employee_number}</td>
              <td>
                <Link to={`/sales/salesperson/edit/${salesperson.id}`}>
                  <Button color="primary">Edit</Button>
                </Link>
                <Button color="danger" onClick={() => handleDelete(salesperson.id)}>Delete</Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
      <NavLink type="button" className='btn btn-warning' to="/sales">Back</NavLink>
    </div>
  );

}

export default SalesPersonList;
