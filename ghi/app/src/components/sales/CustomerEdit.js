import React, { useState, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';

function CustomerEdit() {
    const navigate = useNavigate()
    // use the useParams hook to get the id from the URL
    const { id } = useParams();

    // create a state variable to hold the customer details
    const [customer, setCustomer] = useState({});

    // use useEffect to fetch the customer data from the server when the component is first rendered
    useEffect(() => {
        const fetchData = async () => {
            try {
                // specify the url to get the customer data
                const customerUrl = `http://localhost:8090/api/customers/${id}`;
                // make a GET request to the server
                const response = await fetch(customerUrl);
                // parse the response as json
                const data = await response.json();
                // update the state variable with the customer data
                setCustomer(data);

            } catch (error) {
                console.error(error);

            }

        };
        // call the fetchData function
        fetchData();
    }, [id]); // include the id in the dependency array

    // handle the form submission
    const handleSubmit = async (event) => {
        event.preventDefault();
        try {
            // specify the url to update the customer data
            const updateUrl = `http://localhost:8090/api/customers/${id}`;
            // make a PUT request to the server
            await fetch(updateUrl, {
                method: 'PUT',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(customer)
            });
            // redirect the user to the list page
            navigate('/sales')
        } catch (error) {
            console.error(error);
        }
    };


    return (
        <div className='container p-2'>
            <form onSubmit={handleSubmit}>
                <div className='form-group'>
                    <label htmlFor='name'>Name</label>
                    <input
                        type='text'
                        className='form-control'
                        id='name'
                        defaultValue={customer.name}
                        onChange={e => setCustomer({...customer, name: e.target.value})}
                    />
                </div>
                <div className='form-group'>
                    <label htmlFor='address'>Address</label>
                    <input
                        type='text'
                        className='form-control'
                        id='address'
                        defaultValue={customer.address}
                        onChange={e => setCustomer({...customer, address: e.target.value})}
                    />
                </div>
                <div className='form-group'>
                    <label htmlFor='phone'>Phone</label>
                    <input
                        type='text'
                        className='form-control'
                        id='phone'
                        defaultValue={customer.phone}
                        onChange={e => setCustomer({...customer, phone: e.target.value})}
                    />
                </div>
                <button type='submit' className='btn btn-primary'>Save Changes</button>
            </form>
        </div>
    );
    }
    export default CustomerEdit;
