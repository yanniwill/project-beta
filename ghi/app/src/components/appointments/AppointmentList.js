import React, { useEffect, useState } from "react"
import { NavLink } from "react-router-dom"
import AppointmentDetail from "./AppointmentDetail"
import StatusList from "./StatusList"
import StatusForm from "./StatusForm"
import {BsFillPencilFill, BsToggleOn, BsPatchCheckFill, BsPatchMinusFill} from "react-icons/bs"

function AppointmentList() {
    // Saving html elements into variables to change class names
    const detailViewTag = document.querySelector('#detail-view')
    const statusView = document.querySelector('#status-view')
    const statusViewButton = document.querySelector('#status-view-button')
    // Setting up Hooks
    const [appointments, setAppointments] = useState([])
    const [autos, setAutos] = useState([])
    const [detailId, setDetailId] = useState({
        detailToggle: false
    })
    const [query, setQuery] = useState([])
    // How to translate datetime format into string
    function toLocale(date) {
        let dateStr = new Date(date)
        const options = {
            day: "2-digit",
            month: "2-digit",
            year: "2-digit"
        }
        return dateStr.toLocaleDateString([], options)
    }
    function toTime(date) {
        let dateStr = new Date(date)
        const options = {
            hour: "numeric",
            minute: "numeric"
        }
        return dateStr.toLocaleTimeString([], options)
    }
    // If VIP
    function isVIP(vin) {
        for (let auto of autos) {
            if (vin === auto.vin) {
                return (<BsPatchCheckFill className="text-success"/>)
            }
        }
        return (<BsPatchMinusFill className="text-danger"/>)
    }
    // Handle edit button
    const handleEdit = event => {
        setDetailId({detailId:event.currentTarget.value, detailToggle:true})
    }
    // Handle Status panel info
    const handleStatusButton = () => {
        statusView.classList.remove('d-none')
        statusViewButton.classList.add('d-none')
    }
    const handleStatusHide = () => {
        statusView.classList.add('d-none')
        statusViewButton.classList.remove('d-none')
    }
    // Handle search query
    const handleSearch = event => {
        SearchBy(event.target.value, appointments)
    }
    // Handle search filter
    function SearchBy(q, list) {
        if (!q) {
            setQuery(list)
        }
        else {
            let result = []
            appointments.map(appointment => {
                if (appointment.vin.includes(q.toUpperCase())) {
                    result.push(appointment)
                }
                if (appointment.owner.includes(q)) {
                    result.push(appointment)
                }
                if (appointment.technician.includes(q)) {
                    result.push(appointment)
                }
            })
            let unique = [...new Set(result)]
            setQuery(unique)
        }
    }
    // How to fetch list of automobiles
    const getAutos = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles')
        if (response.ok) {
            const data = await response.json()
            setAutos(data.autos)
        }
    }
    // How to fetch list of appointments
    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments')
        if (response.ok) {
            const data = await response.json()
            setAppointments(data.appointments)
            setQuery(data.appointments)
        }
    }
    // Get list of appointments and automobiles once
    useEffect(() => {
        getData()
        getAutos()
    }, [])
    if (detailId.detailToggle === true) {
        detailViewTag.classList.remove('d-none')
    }
    return (
        <div className="d-grid container">
            <div className="row">
                <div className="container p-2 col">
                    <div className="card shadow p-2" id="list-appointments">
                        <div className="card-title p-2 d-flex justify-content-between">
                            <h1><strong>APPOINTMENTS</strong></h1>
                            <div className="form-floating w-50">
                                <div className="search-wrapper">
                                    <input onChange={handleSearch} placeholder="search..." className="form-control border-success bg-light"/>
                                </div>
                            </div>
                            <div className="text-end">
                                <NavLink className="btn btn-primary" to="new">
                                    <div>
                                        <BsFillPencilFill style={{color:'white', fontSize:'25px'}}/>
                                    </div>
                                </NavLink>
                            </div>
                        </div>
                        <div className="card-body p-2">
                            <table className="table table-hover">
                                <thead>
                                    <tr>
                                        <th>VIN#</th>
                                        <th>OWNER</th>
                                        <th>TECHNICIAN</th>
                                        <th>DATE</th>
                                        <th>TIME</th>
                                        <th>STATUS</th>
                                        <th>VIP</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {query.map(app => {
                                        return (
                                            <tr key={app.id}>
                                                <td>{app.vin}</td>
                                                <td>{app.owner}</td>
                                                <td>{app.technician}</td>
                                                <td>{toLocale(app.date)}</td>
                                                <td>{toTime(app.date)}</td>
                                                <td>{app.status}</td>
                                                <td>{isVIP(app.vin)}</td>
                                                <td>
                                                    <button onClick={handleEdit} value={app.id} className="btn btn-outline-info"><BsToggleOn/></button>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>
                            <div>
                                <button onClick={handleStatusButton} className="btn btn-outline-info" id="status-view-button">
                                    Status info
                                </button>
                            </div>
                            <div className="p-2 m-2 d-none" id="status-view">
                                <div className="m-2">
                                    <button onClick={handleStatusHide} className="btn btn-outline-info">Hide</button>
                                </div>
                                <div className="row p-2 m-2">
                                    <div className="col">
                                        <StatusList/>
                                    </div>
                                    <div className="col" id="status-form-view">
                                        <StatusForm/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col container p-2 d-none" id="detail-view">
                    <AppointmentDetail id={detailId.detailId} autos={autos}/>
                </div>

            </div>
        </div>
    )
}

export default AppointmentList
