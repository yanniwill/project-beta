import { useState, useEffect } from "react"


function AppointmentEdit(props) {
    // Saving properties into variable
    const appointment = props.value
    const detailId = props.id
    // Setting up Hooks
    const [formData, setFormData] = useState({})
    const [technicians, setTechnicians] = useState([])
    const [status, setStatus] = useState([])
    // How to fetch list of status'
    const getStatus = async () => {
        const url = 'http://localhost:8080/api/appointments/status'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setStatus(data.status)
        }
    }
    // How to fetch list of employees
    const getData = async () => {
        const url = 'http://localhost:8090/api/salesperson'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setTechnicians(data.employees)
        }
    }
    // Fetching list of status' and employees
    useEffect(() => {
        getData()
        getStatus()
    },[])
    // How to handle form data
    const handleFormData = event => {
        setFormData({
            ...formData,
            [event.target.name]:event.target.value
        })
        console.log(formData)
    }
    // How to handle submit button
    const handleSubmit = event => {
        event.preventDefault()
        if (!formData.owner) {
            formData['owner'] = appointment.owner
        }
        if (!formData.technician) {
            formData['technician'] = appointment.technician.import_href
        }
        if (!formData.date) {
            formData['date'] = appointment.date
        }
        if (!formData.vin) {
            formData['vin'] = appointment.vin
        }
        if (!formData.reason) {
            formData['reason'] = appointment.reason
        }
        editData()
    }
    // How to post edits to api
    const editData = async () => {
        const url = `http://localhost:8080/api/appointments/${detailId}`
        console.log(formData)
        const fetchConfigs = {
            method: "put",
            body: JSON.stringify(formData),
            headers: {"Content-Type":"application/json"}
        }
        const response = await fetch(url, fetchConfigs)
        if (response.ok) {
            const newResponse = await response.json()
            window.location.reload(false)
        }
    }
    return (
        <div>
            <div className="alert alert-info p-2 d-none" id="edited-message">Successfully edited!</div>
            <div className="d-none" id='edit-appointment'>
                <div className="card p-2">
                    <form onSubmit={handleSubmit}>
                        <div className="card-body">
                            <div className="row">
                                <div className="form-floating col p-2">
                                    <input onChange={handleFormData} name="owner" className="form-control" defaultValue={appointment.owner}/>
                                    <label>Owner</label>
                                </div>
                                <div className="form-floating col p-2">
                                    <select onChange={handleFormData} name="technician" className="form-select">
                                        <option value={appointment.technician.import_href}>{appointment.technician.employee_name}</option>
                                        {technicians.map(tech => {
                                            return (
                                                <option key={tech.id} value={tech.href}>{tech.employee_name}</option>
                                            )
                                        })}
                                    </select>
                                    <label className="form-label">Technician</label>
                                </div>
                                <div className="form-floating p-2">
                                       <input onChange={handleFormData} name="date" type="datetime-local" id="appointment-date" className="form-control" defaultValue={appointment.date}/>
                                       <label>Date</label>
                                </div>
                                <div className="p-2">
                                    <div className="form-floating">
                                        <input onChange={handleFormData} name="vin" className="form-control" defaultValue={appointment.vin}/>
                                        <label>VIN number</label>
                                    </div>
                                </div>
                                <div className="p-2">
                                    <div className="form-floating">
                                        <input onChange={handleFormData} name="reason" type="text" className="form-control" defaultValue={appointment.reason}/>
                                        <label>Reason</label>
                                    </div>
                                </div>
                                <div className="p-2">
                                    <div className="form-floating">
                                        <select onChange={handleFormData} className="form-select" name="status">
                                            <option value={appointment.status.state}>{appointment.status.state}</option>
                                            {status.map(state => {
                                                return (
                                                    <option value={state.state} key={state.state}>{state.state}</option>
                                                )
                                            })}
                                        </select>
                                        <label>Status</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <button className="btn btn-primary w-100">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default AppointmentEdit
