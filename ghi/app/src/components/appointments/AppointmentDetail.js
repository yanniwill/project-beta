import { useState, useEffect } from "react"
import AppointmentEdit from "./AppointmentEdit"
import AppointmentDelete from "./AppointmentDelete"
import {
    BsPencilSquare,
    BsToggle2Off, BsTrash,
    BsPatchCheckFill,
    BsPatchMinusFill
} from 'react-icons/bs'

function AppointmentDetail(props) {
    // Saving properties into variables
    const appointmentId = props.id
    const autos = props.autos
    // Setting up Hooks
    const [appointment, setAppointment] = useState({
        status: {state: "undefined"},
        technician: {employee_name: "undefined"}
        // Did this to resolve issue of page not loading due to no status or technician value to read
    })
    const [vehicles, setVehicles] = useState([])
    // Saving html elements into variables
    const editButton = document.querySelector('#edit-button')
    const editView = document.querySelector('#edit-appointment')
    const cancelButton = document.querySelector('#cancel-button')
    const deleteButton = document.querySelector('#delete-button')
    const deleteView = document.querySelector('#delete-appointment')
    const detailInfo = document.querySelector('#detail-info')
    // How to handle edit button
    const handleEdit = () => {
        editButton.classList.add('d-none')
        detailInfo.classList.add('d-none')
        editView.classList.remove('d-none')
        cancelButton.classList.remove('d-none')
    }
    // How to handle cancel button for edit view
    const handleCancel = () => {
        editButton.classList.remove('d-none')
        detailInfo.classList.remove('d-none')
        editView.classList.add('d-none')
        cancelButton.classList.add('d-none')
    }
    // How to handle delete button
    const handleDelete = () => {
        deleteButton.classList.add('d-none')
        deleteView.classList.remove('d-none')
    }
    // How to handle exit button for detail view
    const handleExit = () => {
        const viewTag = document.querySelector('#detail-view')
        viewTag.classList.add('d-none')
    }
    // How to check if VIN is VIP
    function isVIP(vin) {
        for (let auto of autos) {
            if (vin === auto.vin) {
                return (<BsPatchCheckFill className="text-success" style={{fontSize:"50px"}}/>)
            }
        }
        return (<BsPatchMinusFill className="text-danger" style={{fontSize:"50px"}}/>)
    }
    // How to get the vehicle model picture to populate detail view
    function getPicture(vin, vehicles) {
        let result = "https://images.ebizautos.media/foundation/controls/vehicle-icons/default-sedan.jpg"
        for (let vehicle of vehicles) {
            if (vin === vehicle.vin) {
                result = vehicle.model.picture_url
            }
        }
        return result
    }
    // How to get the details of an appointment
    const getData = async () => {
        const url = `http://localhost:8080/api/appointments/${appointmentId}`
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setAppointment(data[0])
        }
    }
    // How to get list of vehicle models
    const getVehicles = async () => {
        const url = 'http://localhost:8100/api/automobiles'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setVehicles(data.autos)
        }
    }
    // Getting list of vehicle models
    useEffect(() => {
        getVehicles()
    },[])
    // Getting details of appointments everytime props.id changes
    useEffect(() => {
        if (appointmentId) {
            getData()
        }
    },[appointmentId])
    // How to handle date formating
    function toLocale(date) {
        let dateStr = new Date(date)
        const options = {
            weekday: "short",
            month: "short",
            day: "numeric"
        }
        return dateStr.toLocaleDateString("en-US", options)
    }
    function toTime(date) {
        let dateStr = new Date(date)
        const options = {
            hour: "2-digit",
            minute: "2-digit"
        }
        return dateStr.toLocaleTimeString("en-US", options)
    }
    if (appointment) {
    return (
        <div>
            <div className="container shadow p-2" id="detail-appointment">
                <div className="card p-2">
                    <div className="d-grid">
                        <div className="row">
                            <label className="text-muted">VIN number:</label>
                            <h4 className="card col m-2 p-2">{appointment.vin}</h4>
                            <div className="text-end col m2- p-2">
                                <button onClick={handleEdit} className="btn btn-success" id="edit-button">
                                    <BsPencilSquare style={{fontSize:"25px"}}/>
                                </button>
                                <button onClick={handleDelete} className="btn btn-danger m-2" id="delete-button">
                                    <BsTrash style={{fontSize:"25px"}}/>
                                </button>

                                <button onClick={handleExit} className="btn btn-outline-info">
                                    <BsToggle2Off style={{fontSize:"25px"}}/>
                                </button>
                            </div>
                            <button onClick={handleCancel} className="btn btn-warning w-100 d-none" id="cancel-button">Cancel</button>
                            <AppointmentDelete id={appointment.id}/>
                        </div>
                    </div>
                    <div className="card-body">
                        <img className="card-img-top p-2" src={getPicture(appointment.vin, vehicles)}></img>
                        <div className="row" id="detail-info">
                            <div className="col">
                                <div className="card p-2">
                                    <div className="m-2">
                                        <div className="text-muted">Owner</div>
                                        <h5 className="card p-2">{appointment.owner}</h5>
                                    </div>
                                    <div className="m-2">
                                        <div className="text-muted">Appointment date</div>
                                        <div className="card p-2">
                                            <div className="row">
                                                <div className="card-subtitle col text-start">{toLocale(appointment.date)}</div>
                                                <div className="card-subtitle col text-end">{toTime(appointment.date)}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="p-2 card-footer">
                                        <label className="text-muted">VIP Status</label>
                                        <div className="container">
                                            <div className="p-2 text-center rounded">{isVIP(appointment.vin)}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col card p-2">
                                <div className="m-2">
                                    <div className="text-muted">Technician</div>
                                    <h5 className="p-2 card">{appointment.technician.employee_name}</h5>
                                </div>
                                <div className="m-2">
                                    <label className="text-muted">Comments:</label>
                                    <div className="card p-2">{appointment.reason}</div>
                                </div>
                                <div className="p-2 card-footer">
                                    <label className="text-muted">Status</label>
                                    <div className="text-center bg-warning rounded p-2">{appointment.status.state}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <AppointmentEdit id={appointment.id} value={appointment}/>
            </div>
        </div>
    )
}}

export default AppointmentDetail
