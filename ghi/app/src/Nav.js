import { NavLink } from 'react-router-dom';
import {BsPersonCircle} from 'react-icons/bs'

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/"><strong>CarCar</strong></NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mx-auto">
            <li className='nav-item'>
              <NavLink className='nav-link' to='/inventory'>INVENTORY</NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className='nav-link' to='/appointments'>SERVICE</NavLink>
            </li>
            <li className='nav-item'>
              <NavLink className='nav-link' to='/sales'>SALES</NavLink>
            </li>
          </ul>
          <ul className='navbar-nav'>
            <li className='nav-item'>
              <NavLink className='nav-link' to='/accounts'>
                <BsPersonCircle style={{fontSize:"25px"}}/>
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
