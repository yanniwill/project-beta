function Footer() {
    return (
            <div className="bg-success">
                <div className="vw-100 p-2">
                    <div className="text-light text-center p-2">
                        <strong>CarCar@2033 | Public & Lawless | Careers | Newsfeed | Blogs | Locations</strong>
                    </div>
                </div>
            </div>
    )
}

export default Footer
